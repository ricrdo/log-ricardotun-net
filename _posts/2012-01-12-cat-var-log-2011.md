---
layout: post
title: cat /var/log/2011.log
tags: [Personal]
---

2011, se fue muy rápido el año, pareciera que fue hace un mes cuando asistí al SXSWi, hace dos semanas que estuve en Puebla y la pasada en el CPMX3.

¿Qué viene en el 2012
---------------------

No acostumbro a ponerme propósitos, pienso que para hacer algo lo mejor es ponerse metas.

Sin embargo en este año una de las cosas que pretendo es mejorar mis habilidades como programador, me gustaría viajar al igual que en el 2011 y conocer más lugares. Por otro lado quiero echar a andar varios proyectos y también me gustaría colaborar en algún proyecto opensource.

Personalmente, tengo algunas cosas que arreglar. Mientras tanto ¡Adiós 2011! ¡Hola 2012!

