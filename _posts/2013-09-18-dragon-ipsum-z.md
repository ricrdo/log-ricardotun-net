---
layout: post
title: Dragon Ipsum Z
tags: [Projects]
---

Siempre ocupo texto de relleno en el trabajo o en cualquier proyecto en general.

El otro día me encontraba buscando un plugin y de paso me encontré con varios [generadores lorem ipsum](http://chooseyouripsum.com/), me di cuenta de que no había uno de [Dragon Ball Z](http://dragonball.wikia.com/wiki/Dragon_Ball_Z) y en un rato de ocio me di a la tarea de crear uno.

Por el momento es algo muy sencillo pero en ratos libres estaré puliendo y agregando unas cuantas funcionalidades más, entre ellas generar una API para que cada quien sea libre de crear plugins para su editor favorito.

Lo pueden encontrar en [dragonipsumz.herokuapp.com](https://dragonipsumz.herokuapp.com) y pueden [reportar bugs , solicitar features](https://github.com/kelevrium/dragonipsumz/issues) o [pull requests](https://github.com/kelevrium/dragonipsumz/pulls) en [github](https://github.com/kelevrium/dragonipsumz).

![Dragon Ipsum Z]({{ site.upload }}dragon-ipsum-z.png)