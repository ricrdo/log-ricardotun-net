---
layout: post
title: Hello World!
tags: [General, English]
---

I lost the count of my "Hello World!" posts, but this time I won't let this site so abandoned.

This year has been a great year. I got a good job as web developer, I've been involved in some local projects, I met too many people, and there are too many things to come.

I'll write some post in english because I need to practice, as well as write in spanish for the same reason. Also I'll be sharing some things about programming, linux, music, pictures and some other stuff.

That's all for now, welcome again.
