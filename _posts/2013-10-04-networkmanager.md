---
layout: post
title: Conflictos con NetworkManager
tags: [Linux, Arch Linux]
---

Estuve usando mi laptop con un entorno muy simple: herbstluftwm y herramientas en consola.
Finalmente me decidí a instalar Gnome que por defecto usa NetworkManager, pero tenía problemas para conectarme a cualquier read, sea wireless o ethernet.

La solución es simple, se tienen que deshabilitar y parar los servicios de red para que no hagan conflicto con NetworkManager.
El procedimiento que realicé fue el siguiente:

Para listar los servicios corriendo:

    # systemctl --type=service

Para listar los servicios que inician en el arranque del sistema:

    # systemctl list-unit-files|grep enabled

En mi caso eran dos: `dhcpcd` y `netctl`. Dependiendo de lo que necesiten deshabilitar:

Deteniendo servicios:

    # systemctl stop dhcpcd.service
    # systenctl stop netctl.service

Eliminando el inicio del servicio en el arranque:

    # systemctl disable dhcpcd.service
    # systenctl disable netctl.service

Y voailá, ya no debe de haber ningún problema, sólo queda reiniciar, y habilitar el inicio de NetworkManager en el arranque del sistema.
