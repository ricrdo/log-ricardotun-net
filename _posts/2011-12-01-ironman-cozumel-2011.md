---
layout: post
title: Ironman Cozumel 2011, Proyecto y recuento de daños
tags: [Projects, tweetczm]
---

Esta es la 3ra edición del Ford Ironman Cozumel, lamentablemente sólo los habitantes de la isla y algunos visitantes pueden vivir la experiencia. Debido a esto que surge la idea de proveer un lugar en donde se pueda concentrar la información generada por distintas fuentes y junto con Alex, se puso en marcha la tarea de materializar la idea.

<iframe width="853" height="480" src="https://www.youtube.com/embed/OmuvBCL1lC8?rel=0&#038;autohide=1" frameborder="0" allowfullscreen></iframe>

El año pasado fue un simple script en PHP, pero esta vez lo hicimos (casi) una plataforma para darle seguimiento al evento. Es interesante la forma en que distintos servicios proveen APIs de sus servicios y la infinidad de cosas que uno puede hacer. Gracias a esto fue posible este proyecto.

En general lo que se hizo fue una búsqueda en twitter, los resultados eran clasificados sí el contenido del tweet era sólo texto o si incluían algún link, después de esto eran clasificados nuevamente según el servicio utilizado para subir la imagen.

Benchmarks
----------

**2010**

Script en PHP, sólo generaba un json y los resultados se imprimían con JavaScript, sencillo y rústico pero funcionaba. Las imágenes se repetían si alguien daba RT, se abusó mucho del servicio al no tener validación de ningún tipo. Los resultados se refrescaban con JavaScript y sobre escribía lo visto en pantalla.
Sólo soportaban las imágenes de TwitPic, yFrog y Plixi (ahora Lockerz).

**2011**

Escrito en Python, usando Django y la información se guardaba en una base de datos. Usando algo del lado del servidor pudimos hacer mucho más cosas, entre ellas validar las URLs, se estableció que no haya duplicados de ellas, con eso se resolvió el problema de los RT’s y también el abuso del servicio.
Los resultados se hacían en background con un cron job, o sea que se volvió “transparente” al usuario y con bondades de ajax se pudo mejorar la interfaz.
Se hicieron dos versiones, una para desktop y otra para mobile, se detectaba del lado del servidor para saber qué vista devolver al cliente.
Servicios soportados de imágenes: Twitpic, yFrog, Lockerz, Instagram, Twitter (nuevo servicio de fotos) y esta vez también se soportaron servicios de video, en efecto YouTube y TwitVid.
Entre otras cosas, se hizo un blog, una administración de patrocinadores, gestión de páginas y analytics de los tweets.

El trabajo final
----------------

![Demo App Ironman 2011 tweetczm]({{ site.upload }}demo-app-ironman-2011-tweetczm.jpg)

Ya en ejecución la aplicación funcionó de maravilla, salvo que por ~15 minutos se cayó el servicio de MySQL, aunque no afectó en nada, ya que la búsqueda de tweets la hace desde el id del último tweet introducido a la base de datos, debido a eso prácticamente no se perdió ninguna foto.
El año pasado hubieron errores que reparamos este año, sin embargo este año también salieron algunos detalles, aunque empezamos a desarrollar la aplicación 2 meses antes, al final nos comió un poco el tiempo y algunas cosas pudieron haber quedado mejor.

La experiencia
--------------

A comparación del año pasado, esta vez disfruté mejor el Ironman, tomando la experiencia del año pasado esta vez planeamos mejor la logística, no hubo vehículos varados, hubo comida y también participaron más personas.
La pre organización/producción estuvo a cargo de [@keuniceg](http://twitter.com/keuniceg), [@maparimx](http://twitter.com/maparimx), [@Chente139](http://twitter.com/Chente139), [@robertohreyes](http://twitter.com/robertohreyes), [@iAlex_](http://twitter.com/ialex_) y un servidor.
Este año recibimos la visita y el apoyo de la comunidad de [@TwittPlaya](http://twitter.com/twittplaya) y [@VivoEnPlaya](http://twitter.com/VivoEnPlaya), junto con [@Warcl](http://twitter.com/Warcl), [@albert_montoya](http://twitter.com/albert_montoya), [@Pamolita](http://twitter.com/Pamolita) y [@stencilblack](http://twitter.com/stencilblack).
Por parte de [@tweetczm](http://twitter.com/tweetczm) participaron: [@SalaXia](http://twitter.com/SalaXia), [@PsyGoat](http://twitter.com/PsyGoat), [@Diegwoody](http://twitter.com/Diegwoody), [@MelinaaFdz](http://twitter.com/MelinaaFdz), [@mermib](http://twitter.com/mermib) y [@cruz_coz](http://twitter.com/cruz_coz).
Creo que todos nos llevamos una buena experiencia y en lo personal creo que salió mejor de lo que teníamos pensado.

Estadísticas
------------

Alex se encargó de sacar algunas estadísticas de la información que recopilamos durante el evento, en resumen:

**Total de Fotos: 659**

-  Yfrog: 264
-  Twitter: 170
-  Lockerz: 101
-  Twitpic: 117
-  Instagram: 7
-  Total de Videos: 30
-  Youtube: 22
-  Twitvid: 7

**Usuarios que subieron más fotos:**

-  @VivoenPlaya: 39
-  @varito_coz: 33
-  @mermib: 31
-  @marrovni: 25
-  @PsyGoat: 23

**Del 100% de visitas el 45% se generaron desde dispositivos móviles, de las cuales:**

-  iPhone: 39.70%
-  iPad: 23.60%
-  BlackBerry: 15.73%
-  Android: 13.86%
-  iPod: 6.74%
-  SymbianOS: 0.37%

Espero que podamos tener estadísticas de otros factores la próxima vez.

Y en conclusión eso fue el Ford Ironman Cozumel 2011 desde un punto no tan deportivo, gracias a todos los que participaron y también agradezco mucho a las personas que apoyaron el proyecto directa o indirectamente.
¡Nos vemos en el Ironman 70.3 en septiembre y el Ironman de Noviembre!