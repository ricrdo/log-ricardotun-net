---
layout: post
title: Graduación UNID generación 2007-2010
tags: [Personal]
---

Quiero felicitar a los nuevos Licenciados: [Vicente Rodríguez](http://twitter.com/Chente139), [Joaquín Rivero](http://twitter.com/joaquinrivero), [Laura Valladares](http://twitter.com/LauraAVS) y en especial a mi novia [Eunice Gallegos](http://twitter.com/keuniceg).

¡Les deseo mucho éxito! El próximo año me toca a mí.
