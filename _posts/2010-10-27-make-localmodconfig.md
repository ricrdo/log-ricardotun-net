---
layout: post
title: make localmodconfig
tags: [Linux, Kernel]
---

Vamos a compilar el kernel pero no tenemos un respaldo del `.config` o no tenemos la configuración para la máquina donde actualizaremos el kernel. Podríamos usar `make menuconfig` y escoger manualmente los módulos y características que necesitamos; A partir del kernel 2.6.32 esta tarea se vuelve mucho más sencilla usando `make localmodconfig`.

Básicamente lo que hace es ejecutar un `lsmod` para detectar los módulos que están cargados en el sistema, junto con sus configuraciones y dependencias para generar un `.config`; Yo les recomiendo ejecutar después un `make menuconfig` para verificar que no falte nada, por ejemplo, tuve que habilitar manualmente el módulo para mi webcam y de paso des-habilité ipv6.

Y ahora sí... ¡A compilar!
