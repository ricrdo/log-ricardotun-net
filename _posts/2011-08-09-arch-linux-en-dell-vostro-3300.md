---
layout: post
title: Arch Linux en Dell Vostro 3300
tags: [Linux, Arch Linux]
---

Ya tengo un año con esta laptop, apenas la tuve en mis manos lo primero que hice fue borrar por completo windows (incluso borré la partición de restauración) para ponerle debian. Después de un año y ya con tanta basura en el sistema decidí poner arch linux. Ya había usado antes esta distribución en mi equipo anterior, una AA1.

Realicé una instalación por red, así que al terminar tenía paquetes frescos y no tuve ningún problema, me reconoció absolutamente todo el hardware, incluso la tarjeta inalámbrica.

Estoy usando Gnome 3 y después de instalar las aplicaciones que uso cotidianamente no me había dado cuenta de que no tenía alsa en mi sistema. Iba todo bien hasta después de reiniciar el sistema, el audio desapareció, al correr `alsaconf` el demonio no iniciaba y el único error era de que no encontraba ninguna tarjeta.

La solución está en agregar manualmente nuestro chipset editando el archivo `/var/tmp/alsaconf.cards` después de donde dice `snd-hda-intel.o;` quedaría más o menos así:

	snd-hda-intel.o PCI: 0x8086=0x3b56

Nota: los números exadecimales lo saqué de la salida de lspci -nn

	00:1b.0 Audio device [0403]: Intel Corporation 5 Series/3400 Series Chipset High Definition Audio [8086:3b56] (rev 06)

Y voailá.
