---
layout: post
title: Black Sabbath
tags: [Music, Personal]
---

Desde War Pigs hasta Paranoid, sin duda es el mejor concierto al que he asistido.
Durante el concierto un no creía que estaba viendo al mismo Ozzy Osbourne interactuando con el público y a Tony Iommi deleitando con sus sólos en la guitarra.

Nunca creí poder tener la oportunidad de ver en vivo a una banda de antaño, siempre creí que sólo iba poder verlos en DVD.
Ya en mi lista "Cosas que hacer antes de morir" he tachado "Ir a un concierto de una banda legendaria".

![Black Sabbath](http://instagram.com/p/f8vpxrAM6L/media?size=l)

![Ozzy Osbourne](http://instagram.com/p/f9gnYsgM7a/media?size=l)

![Tony Iommi](http://instagram.com/p/f9gD-cgM65/media?size=l)

