---
layout: post
title: Repositorio para VirtualBox 4.x.x
tags: [Linux, Debian]
---


Siempre he preferido instalar software desde repositorios. Cuando algo no se encuentra en los repositorios oficiales de debian me doy a la tarea de buscar un repositorio, casi siempre hay uno, tal es el caso de [Oracle VirtualBox](http://www.virtualbox.org/).

Desde la salida de la versión 4.x.x le daba `apt-get update` y en el search únicamente me mostraba disponible la versión 3.2, el repositorio que tenía es el siguiente:

    deb http://download.virtualbox.org/virtualbox/debian squeeze non-free

Verificando en la sección de [downloads de virtualbox](http://www.virtualbox.org/wiki/Linux_Downloads) me doy cuenta de que el repositorio ha cambiado, sólo hay que cambiar *non-free* por *contrib* quedando de la siguiente manera:

    deb http://download.virtualbox.org/virtualbox/debian squeeze contrib

`apt-get update` y voailá, a actualizar. Si no tienen la llave la pueden descargar y agregar con el siguiente comando:

    wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | sudo apt-key add -
