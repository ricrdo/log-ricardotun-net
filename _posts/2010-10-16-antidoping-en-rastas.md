---
layout: post
title: Antidoping en Rastas
tags: [Music]
---

Hace un poco más de un año que vi a Antidoping en Playa del Carmen, esta vez tuvieron una presentación en Rastas, en la punta sur de la Isla. Fui acompañado de [Israel](http://twitter.com/israelmeneses_) y [Laura](http://twitter.com/lalikkk), así mismo también conseguí el nuevo material discográfico y un autógrafo de Pedro Apodaca, baterísta y fundador de Antidoping.

La presentación fue corta pero desde el inicio estuvieron llenando a todos de vibra positiva y buena energía. Tocaron algo de su nuevo álbum y algunos covers, entre ellos Lively up Yourself de Bob Marley.