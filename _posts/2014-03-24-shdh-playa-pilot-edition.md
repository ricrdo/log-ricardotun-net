---
layout: post
title: SHDH Playa Pilot Edition
tags: [Projects]
---

![@SHDHPlaya](http://instagram.com/p/l301DdgM3r/media?size=l)

Hace unas semanas con [Alejandro Villanueva](https://github.com/ialex) surgió la platica
sobre organizar y llevar acabo algun evento en Playa del Carmen, después
de analizar las opciones nos decidimos por un [Super Happy Dev House](http://http://superhappydevhouse.org/).
Poco después el buen [Alejandro Gómez](https://github.com/alexserver) se integró al equipo durante
el proceso de encontrar un lugar.

Y así fue como el primer [Super Happy Dev House Playa del Carmen](http://shdhplaya.org) se llevó acabo
el pasado sábado 22 de marzo con alrededor de 15 a 20 asistentes. El evento se llevó con éxito,
fue la ocasión excelente para hacer networking, hablar sobre proyectos, compartir tips y experiencias.

![Super Happy Dev House Playa del Carmen](http://instagram.com/p/l301EqAM3s/media?size=l)

Sin duda alguna el [SHDH Playa](http://shdhplaya.org) no hubiera sido posible gracias al apoyo de
[Daniel Alcocer](https://twitter.com/dalcocer) quien nos permitio hacer uso de las instalaciones de
[COFFICE Playa](https://www.facebook.com/pages/COFFICE-Playa/280965838722499).
[Mark Sergienko](https://twitter.com/Mkrn) quien aporto un pequeño desayuno para arrancar el dia.
Y los alimentos que disfrutamos en el amuerzo gracias al patrocinio de [Dennis van der Heijden](https://twitter.com/dennisvdheijden)
de [Convert](http://convert.com).
