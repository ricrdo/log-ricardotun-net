---
layout: post
title: Recopilación de tweets durante la Jornada Electoral 2013 en Cozumel
tags: [Projects, tweetczm]
---

Siempre he tratado de mantenerme alejado de temas políticos, sobre todo porque se me hace muy molesto el ruido visual y sonoro con sus propagandas. Ahora con todo el boom de Internet y Social Media es casi imposible evitarlo.

Por otra parte, desde hace un par de años he tratado de tener un poco de "responsabilidad cívica" y/o al menos mantenerme de lo que ocurre en torno al escenario durante elecciones.

El domingo puse a andar una webapp que me eché en unas horas. En un principio era para fines estadísticos y análisis, pero al final terminó siendo simplemente una recolección de tweets relacionados al evento, esto debido a que al no poder definir un criterio de búsqueda que me permita automatizar la tarea, tuve que recolectarlos manualmente.

Además de esto, aun haciendo una búsqueda en general eran muchas variables o filtros que se podían definir y al tener poco tiempo para realizar pruebas preferí dejar el proyecto así sencillo, ya habrá una próxima vez con más tiempo para desarrollar algo mejor y automático como en el caso del [Ironman](http://log.ricardotun.net/2011/12/01/ironman-cozumel-2011/).

Utilicé [Django](https://www.djangoproject.com) como framework de desarrollo y [Bootstrap](http://twitter.github.com/bootstrap) para la UI (también para hacerla web responsive). 
Para procesar los tweets me apoyé en [statuses/oembed](https://dev.twitter.com/docs/api/1.1/get/statuses/oembed) de la API 1.1 de twitter.

La pueden ver en acción en [elecciones2013.tweetczm.com](http://elecciones2013.tweetczm.com).
El código lo liberé y lo pueden encontrar en [este repositorio en github](https://github.com/kelevrium/election-tweets).

Y como decía, al final no pude recabar o más bien ni si quiera me di la tarea de formular datos estadísticos con el contenido recolectado, pero fue interesante ver la actividad que se llevó a cabo.
