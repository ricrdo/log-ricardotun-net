---
layout: post
title: BCM43224, Kernel 3.0 y Arch Linux
tags: [Linux, Arch Linux]
---

Desde hace 3 o 4 semanas no actualizaba el sistema por terminar proyectos finales de la escuela y desde hace una semana y media no tocaba para nada mi laptop. Después de descansar unos días y hacer otras cosas en la vida real, la tomé de nuevo y lo primero que hice fue actualizarla. Lo más novedoso en el update fue que ya está el Kernel 3.0 en los repositorios, todo bien excepto que la tarjeta inalámbrica no me funcionaba.

Tengo una Broadcom:

	12:00.0 Network controller: Broadcom Corporation BCM43224 802.11a/b/g/n (rev 01)

Antes usaba el módulo `brcm80211`, leyendo me entero que fue renombrado a `brcmsmac`, ejecutando un `lsmod` me doy cuenta de que ya está cargado el módulo pero sigo sin ver mi tarjeta inalámbrica.
El problema es de que el módulo `brcmsmac` tiene conflictos con el módulo `bcma`, así que la solución es ponerlo en lista negra en el archivo `/etc/modprobe.d/modprobe.conf` para que no cargue al iniciar el sistema:

	blacklist bcma

Y voailá…


