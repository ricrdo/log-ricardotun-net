---
layout: post
title: Fin de semana en Mérida
tags: [Personal]
---

Por cuestiones de trámites viajé a Mérida desde el jueves en la noche, aprovechando pasé todo el fin de semana ahí en compañía de [Eunice](http://twitter.com/keuniceg).

Sin querer fueron unas excelentes vacaciones, me relajé bastante y me pude olvidar completamente del trabajo, escuela y otras cosas. Aproveché para realizar compras de fin de año y pude pensar sobre proyectos personales, algunas cosas que pasaron durante el año y cosas que vienen en camino.

Aunque fue breve, fue bueno ver una vez más a [nekrox](http://twitter.com/#!/nekrox). También fue un gusto conocer a [Xtr3m0](http://twitter.com/Xtr3m0), [RoverWire](http://twitter.com/RoverWire) y [Elcanaca_](http://twitter.com/elcanaca_).

Creo que es la primera vez que tengo la oportunidad de visitar Mérida y "turistear", el viaje estuvo bien, regresé con las pilas recargadas y ahora sólo me queda cerrar bien el año.

